--- DOCUMENTAÇÃO DO PROJETO EP1 ---

>> Realização
1) Preparação (pastas e commit inicial);
2) Criação das Classes Necessárias
  2.1) Classe Abstrata GameObject
  2.2) Classes Filhas Player, Trap e Bonus
  2.3) Classe Auxiliar Map
  2.4) Classe Auxiliar Draw
  2.5) Classe Auxiliar Collision
  2.6) Classes Filhas PlayerWallCollision, PlayerTrapCollision, etc.
3) Método movimentar()
4) Implementação do Método posicionaTraps() de Forma Aleatória
5) Inclusão da NCurses
6) Jogo Funcional

>> Principios de OO
  1) Herança
    1.1) GameObject -> Player,Trap e Bonus
    1.2) Collision -> PlayerWallCollision,etc.
  2) Polimorfismo
    2.1) Sobrecarga de métodos: Player possui construtor padrão e específico
    2.2) Sobrescrita de métodos: todas as classes filhas de Collision são obrigadas a implementar os métodos da superclasse.
  3) Abstração
    3.1) Classe Collision

>> Compilação e Execução
  Todo o projeto foi implementado usando um Makefile comum acrescido de "-lncurses". Logo, para a compilação do programa, basta utilizar o comando "make" no terminal dentro da pasta do projeto para compilar e "make run" para executar.
