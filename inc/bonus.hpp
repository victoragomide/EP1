#ifndef BONUS_HPP
#define BONUS_HPP

#include "gameobject.hpp"

class Bonus : public GameObject {
  private:
    int score;
  public:
    Bonus();
    Bonus(int x, int y, char sprite);
    ~Bonus();

    int getScore();
    void setScore(int score);
};

#endif
