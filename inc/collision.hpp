#ifndef COLLISION_HPP
#define COLLISION_HPP

#include <iostream>

#include "map.hpp"
#include "player.hpp"

class Collision {
  public:
    virtual bool detectaCollision(Map fase, Player p1) = 0;
    virtual void resolveCollision(Map fase, Player &p1) = 0;
};

#endif
