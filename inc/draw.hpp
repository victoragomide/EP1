#ifndef DRAW_HPP
#define DRAW_HPP

#include "map.hpp"
#include "player.hpp"
#include "trap.hpp"
#include "bonus.hpp"

class Draw {
  public:
    Draw();
    ~Draw();

    Map posicionaPlayer(Map fase, Player p1);
    Map posicionaTraps(Map fase, Trap traps[], int qtde);
    Map posicionaBonus(Map fase, Bonus bonus[], int qtde);
    GameObject encontraGoal(Map fase);
    void desenhaFase(Map fase);
};

#endif
