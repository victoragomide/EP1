#ifndef GAMEOBJECT_HPP
#define GAMEOBJECT_HPP

#include "map.hpp"

using namespace std;

class GameObject {
    private:
	int x_position, y_position;
	char sprite;
    public:
	GameObject();
  GameObject(int x, int y, char sprite);
	~GameObject();

	int getXPosition();
  void setXPosition(int x);
  int getYPosition();
  void setYPosition(int y);
  char getSprite();
  void setSprite(char sprite);
  void geraPosicaoAleatoriaValida(Map fase);
};

#endif
