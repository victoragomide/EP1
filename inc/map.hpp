#ifndef MAP_HPP
#define MAP_HPP

#define LIN 20
#define COL 50

using namespace std;

class Map {
  public:
    char  matriz[LIN][COL];

    Map();
    ~Map();

    void carregaArquivo();
};

#endif
