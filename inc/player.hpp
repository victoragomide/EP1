#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "gameobject.hpp"

class Player : public GameObject {
  private:
    bool alive;
    int score;
    bool winner;
    int lives;

  public:
    Player();
    Player(int x, int y, char sprite);
    ~Player();

    bool getAlive();
    void setAlive(bool alive);
    int getScore();
    void setScore(int score);
    bool getWinner();
    void setWinner(bool winner);
    int getLives();
    void setLives(int lives);

    void movimentar(char tecla, Map fase_1);
};

#endif
