#ifndef PLAYERBONUSCOLLISION_HPP
#define PLAYERBONUSCOLLISION_HPP

#include "collision.hpp"

class PlayerBonusCollision : public Collision {
  public:
    PlayerBonusCollision();
    ~PlayerBonusCollision();
    bool detectaCollision(Map fase, Player p1);
    void resolveCollision(Map fase, Player &p1);
};

#endif
