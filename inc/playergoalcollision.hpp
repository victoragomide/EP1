#ifndef PLAYERGOALCOLLISION_HPP
#define PLAYERGOALCOLLISION_HPP

#include "collision.hpp"

class PlayerGoalCollision : public Collision {
  public:
    PlayerGoalCollision();
    ~PlayerGoalCollision();
    bool detectaCollision(Map fase, Player p1);
    void resolveCollision(Map fase, Player &p1);
};

#endif
