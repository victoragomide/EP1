#ifndef PLAYERTRAPCOLLISION_HPP
#define PLAYERTRAPCOLLISION_HPP

#include "collision.hpp"

class PlayerTrapCollision : public Collision {
  public:
    PlayerTrapCollision();
    ~PlayerTrapCollision();
    bool detectaCollision(Map fase, Player p1);
    void resolveCollision(Map fase, Player &p1);
};

#endif
