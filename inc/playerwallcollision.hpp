#ifndef PLAYERWALLCOLLISION_HPP
#define PLAYERWALLCOLLISION_HPP

#include "collision.hpp"

class PlayerWallCollision : public Collision {
  public:
    PlayerWallCollision();
    ~PlayerWallCollision();
    bool detectaCollision(Map fase, Player p1);
    void resolveCollision(Map fase, Player &p1);
};

#endif
