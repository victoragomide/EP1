#ifndef TRAP_HPP
#define TRAP_HPP

#include "gameobject.hpp"

class Trap : public GameObject {
  private:
    int damage;
  public:
    Trap();
    Trap(int x, int y, char sprite);
    ~Trap();

    int getDamage();
    void setDamage(int damage);
};

#endif
