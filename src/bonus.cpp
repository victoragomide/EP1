#include "bonus.hpp"

Bonus::Bonus() {
  setXPosition(0);
  setYPosition(0);
  setSprite('$');
  setScore(1);
}

Bonus::Bonus(int x, int y, char sprite) {
  setXPosition(x);
  setYPosition(y);
  setSprite(sprite);
  setScore(1);
}

Bonus::~Bonus() {}

int Bonus::getScore() {
  return score;
}

void Bonus::setScore(int score) {
  this->score = score;
}
