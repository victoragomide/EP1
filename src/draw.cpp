#include "draw.hpp"

#include <iostream>
#include <stdlib.h>
#include <ncurses.h>

Draw::Draw() {}

Draw::~Draw() {}

Map Draw::posicionaPlayer(Map fase, Player p1) {
  fase.matriz[p1.getYPosition()][p1.getXPosition()] = p1.getSprite();
  return fase;
}

Map Draw::posicionaTraps(Map fase, Trap traps[], int qtde) {
  int i;
  for(i=0;i<qtde;i++){
    fase.matriz[traps[i].getYPosition()][traps[i].getXPosition()] = traps[i].getSprite();
  }
  return fase;
}

Map Draw::posicionaBonus(Map fase, Bonus bonus[], int qtde) {
  int i;
  for(i=0;i<qtde;i++){
    fase.matriz[bonus[i].getYPosition()][bonus[i].getXPosition()] = bonus[i].getSprite();
  }
  return fase;
}

void Draw::desenhaFase(Map fase) {
  int i,j;
  for(i=0;i<LIN;i++) {
    for(j=0;j<COL;j++)
      printw("%c",fase.matriz[i][j]);
    printw("\n");
  }
}

GameObject Draw::encontraGoal(Map fase) {
  int i,j;
  GameObject goal;
  for(i=0;i<LIN;i++)
    for(j=0;j<COL;j++)
      if(fase.matriz[i][j] == '8') {
        goal.setXPosition(j);
        goal.setYPosition(i);
        goal.setSprite('8');
      }
  return goal;
}
