#include <stdlib.h>

#include "gameobject.hpp"

GameObject::GameObject() {
  setXPosition(0);
  setYPosition(0);
  setSprite('0');
}

GameObject::GameObject(int x, int y, char sprite) {
  setXPosition(x);
  setYPosition(y);
  this->sprite = sprite;
}

GameObject::~GameObject() {}

int GameObject::getXPosition() {
  return x_position;
}

void GameObject::setXPosition(int x) {
  x_position = x;
}

int GameObject::getYPosition() {
  return y_position;
}

void GameObject::setYPosition(int y) {
  y_position = y;
}

char GameObject::getSprite() {
  return sprite;
}

void GameObject::setSprite(char sprite) {
  this->sprite = sprite;
}

void GameObject::geraPosicaoAleatoriaValida(Map fase) {
  int x,y;
  do {
    x = rand() % (COL-1);
    y = rand() % (LIN-1);
  } while (fase.matriz[y][x] == '=');
  setXPosition(x);
  setYPosition(y);
}
