#include <ncurses.h>
#include <stdlib.h>
#include <time.h>

#include "map.hpp"
#include "draw.hpp"
#include "player.hpp"
#include "trap.hpp"
#include "gameobject.hpp"

#define QTDE_ELEM 5

int main(int argc, char ** argv) {
  srand(time(NULL));
  char tecla='l';
  int i;
  Draw draw_1;
  Map map_1,fase_1;
  Player p1(2,1,'@');
  GameObject goal;
  Trap traps[QTDE_ELEM];
  Bonus bonus[QTDE_ELEM];
  map_1.carregaArquivo();
  goal = draw_1.encontraGoal(map_1);
  for(i=0;i<QTDE_ELEM;i++) {
    traps[i].geraPosicaoAleatoriaValida(map_1);
    bonus[i].geraPosicaoAleatoriaValida(map_1);
  }
  while(true) {
    initscr();
		clear();
		keypad(stdscr, TRUE);
		noecho();
    fase_1 = draw_1.posicionaTraps(map_1,traps,QTDE_ELEM);
    fase_1 = draw_1.posicionaBonus(fase_1,bonus,QTDE_ELEM);
    p1.movimentar(tecla,fase_1);
    fase_1 = draw_1.posicionaPlayer(fase_1,p1);
    draw_1.desenhaFase(fase_1);
    if(tecla=='\n')
      exit(0);
    refresh();
    tecla = getch();
		endwin();
  }
  return 0;
}
