#include <ncurses.h>
#include "player.hpp"

Player::Player() {
  setXPosition(0);
  setYPosition(0);
  setSprite('@');
  setAlive(true);
  setScore(0);
  setWinner(false);
  setLives(3);
}

Player::Player(int x, int y, char sprite) {
  setXPosition(x);
  setYPosition(y);
  setSprite(sprite);
  setAlive(true);
  setScore(0);
  setWinner(false);
  setLives(3);
}

Player::~Player() {}

bool Player::getAlive() {
  return alive;
}

void Player::setAlive(bool alive) {
  this->alive = alive;
}

int Player::getScore() {
  return score;
}

void Player::setScore(int score) {
  this->score = score;
}

bool Player::getWinner() {
  return winner;
}

void Player::setWinner(bool winner) {
  this->winner = winner;
}

int Player::getLives() {
  return lives;
}

void Player::setLives(int lives) {
  this->lives = lives;
}

void Player::movimentar(char tecla, Map fase) {
  int xatual, yatual;
  xatual = getXPosition();
  yatual = getYPosition();
  switch(tecla) {
    case 's':
      if(fase.matriz[yatual+1][xatual] != '=')
        setYPosition(yatual+1);
      break;
    case 'w':
      if(fase.matriz[yatual-1][xatual] != '=')
        setYPosition(yatual-1);
      break;
    case 'd':
      if(fase.matriz[yatual][xatual+1] != '=')
        setXPosition(xatual+1);
      break;
    case 'a':
      if(fase.matriz[yatual][xatual-1] != '=')
        setXPosition(xatual-1);
      break;
  }
}
