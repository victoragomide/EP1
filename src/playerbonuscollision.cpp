#include "playerbonuscollision.hpp"

PlayerBonusCollision::PlayerBonusCollision() {}

PlayerBonusCollision::~PlayerBonusCollision() {}

bool PlayerBonusCollision::detectaCollision(Map fase, Player p1) {
  if(fase.matriz[p1.getYPosition()][p1.getXPosition()] == '$')
    return true;
  else
    return false;
}

void PlayerBonusCollision::resolveCollision(Map fase, Player &p1) {
  if(detectaCollision(fase,p1)) {
    p1.setScore(p1.getScore()+1);
  }
}
