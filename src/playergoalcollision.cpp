#include "playergoalcollision.hpp"

PlayerGoalCollision::PlayerGoalCollision() {}

PlayerGoalCollision::~PlayerGoalCollision() {}

bool PlayerGoalCollision::detectaCollision(Map fase, Player p1) {
  if(fase.matriz[p1.getYPosition()][p1.getXPosition()] == '8')
    return true;
  else
    return false;
}

void PlayerGoalCollision::resolveCollision(Map fase, Player &p1) {
  if(detectaCollision(fase,p1)) {
    p1.setWinner(true);
  }
}
