#include "playertrapcollision.hpp"

PlayerTrapCollision::PlayerTrapCollision() {}

PlayerTrapCollision::~PlayerTrapCollision() {}

bool PlayerTrapCollision::detectaCollision(Map fase, Player p1) {
  if(fase.matriz[p1.getYPosition()][p1.getXPosition()] == '%')
    return true;
  else
    return false;
}

void PlayerTrapCollision::resolveCollision(Map fase, Player &p1) {
  if(detectaCollision(fase,p1)) {
    p1.setLives(p1.getLives()-1);
    if(p1.getLives()==0) {
      p1.setAlive(false);
    }
  }
}
