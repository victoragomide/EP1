#include "trap.hpp"

Trap::Trap() {
  setXPosition(0);
  setYPosition(0);
  setSprite('%');
  setDamage(1);
}

Trap::Trap(int x, int y, char sprite) {
  setXPosition(x);
  setYPosition(y);
  setSprite(sprite);
  setDamage(1);
}

Trap::~Trap() {}

int Trap::getDamage() {
  return damage;
}

void Trap::setDamage(int damage) {
  this->damage = damage;
}
